﻿using UnityEngine;
using System.Collections;

public class KeyBindings : MonoBehaviour
{
	public KeyCode throttleUp = KeyCode.W;
	public KeyCode throttleDown = KeyCode.S;
	public KeyCode thrustUp = KeyCode.R;
	public KeyCode thrustDown = KeyCode.F;
	public KeyCode thrustRight = KeyCode.D;
	public KeyCode thrustLeft = KeyCode.A;
	
	public KeyCode enterShip = KeyCode.X;
}
