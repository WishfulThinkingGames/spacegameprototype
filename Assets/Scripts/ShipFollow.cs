﻿using UnityEngine;
using System.Collections;

public class ShipFollow : MonoBehaviour
{
	public bool matchRotation = true;
	public GameObject realShip;

	// Update is called once per frame
	void Update()
	{
		transform.position = realShip.transform.position;

		if (matchRotation)
		{
			transform.rotation = realShip.transform.rotation;
		}
	}
}
