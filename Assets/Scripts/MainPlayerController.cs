﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class MainPlayerController : MonoBehaviour
{
	#region Settings

	public GameObject fakePlayerPrefab;	
	public bool gravOnExit;

	public bool lockCursor = true;
	private KeyBindings keys;
	#endregion

	#region Objects/Components

	private Rigidbody rb;
	private GameObject fakePlayer;
	private GameObject fakeShip;
	#endregion

	#region Varibles

	public bool onShip;
	public bool inControlPoint;
	public bool inShipControl;

	#region Ship Control Variables

	public GameObject shipToControl;
	public ShipController shipSettings;
	public Rigidbody shipRB;

	private ControlPointSettings ctrlSettings;
	#endregion
	#endregion

	public Vector3 currentAngularVel;

	void Start()
	{
		rb = GetComponent<Rigidbody>();
		keys = GetComponent<KeyBindings>();

		if (lockCursor)
		{
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
		}
	}

	void Update()
	{
		if (inControlPoint && Input.GetKeyDown(keys.enterShip) && !inShipControl)
		{
			TakeControl(ctrlSettings);
		}
		else if (inControlPoint && Input.GetKeyDown(keys.enterShip) && inShipControl)
		{
			LeaveControl(ctrlSettings);
		}

		#region Character Movement
		if (!inShipControl)
		{
			if (!onShip)
			{
				Vector3 currentVel = transform.InverseTransformDirection(rb.velocity);

				Vector3 newVelocity = new Vector3(Input.GetAxis("Horizontal"), currentVel.y, Input.GetAxis("Vertical"));

				if (Input.GetKeyDown("space"))
				{
					newVelocity = new Vector3(rb.velocity.x, 3, rb.velocity.z);
				}
				rb.velocity = transform.TransformDirection(newVelocity);
			}
			else
			{
				rb.velocity = Vector3.zero;
				transform.localPosition = fakePlayer.transform.localPosition;
				transform.localRotation = fakePlayer.transform.localRotation;
			}
		}
		else
		{
			Vector3 shipCurrentVel = shipRB.transform.InverseTransformDirection(shipRB.velocity);

			#region Forward Throttle FA on
			if (Input.GetAxis("ForeBackStick") > 0)
			{
				shipSettings.forwardThrottle = Mathf.Clamp(Input.GetAxis("ForeBackStick"), 0, 1);
			}
			else if (Input.GetKey(keys.throttleUp) && shipSettings.backThrottle == 0)
			{
				shipSettings.forwardThrottle += 0.5f * Time.deltaTime;

				shipSettings.forwardThrottle = Mathf.Clamp(shipSettings.forwardThrottle, 0, 1);
			}
			else if (Input.GetKey(keys.throttleDown))
			{
				shipSettings.forwardThrottle -= 0.5f * Time.deltaTime;

				shipSettings.forwardThrottle = Mathf.Clamp(shipSettings.forwardThrottle, 0, 1);
			}
			#endregion

			#region Backward Throttle FA on
			if (Input.GetAxis("ForeBackStick") < 0)
			{
				shipSettings.backThrottle = Mathf.Clamp(Input.GetAxis("ForeBackStick"), -1, 0);
			}
			else if (Input.GetKey(keys.throttleDown) && shipSettings.forwardThrottle == 0)
			{
				shipSettings.backThrottle = -1;
			}
			else
			{
				shipSettings.backThrottle = 0;
			}
			#endregion

			#region Up Throttle FA on
			if (Input.GetKey(keys.thrustUp))
			{
				shipSettings.upThrottle = 1;
			}
			else
			{
				shipSettings.upThrottle = 0;
			}
			#endregion

			#region Down Throttle FA on
			if (Input.GetKey(keys.thrustDown))
			{
				shipSettings.downThrottle = -1;
			}
			else
			{
				shipSettings.downThrottle = 0;
			}
			#endregion

			#region Right Throttle FA on
			if (Input.GetKey(keys.thrustRight))
			{
				shipSettings.starboardThrottle = 1;
			}
			else
			{
				shipSettings.starboardThrottle = 0;
			}
			#endregion

			#region Left Throttle FA on
			if (Input.GetKey(keys.thrustLeft))
			{
				shipSettings.portThrottle = -1;
			}
			else
			{
				shipSettings.portThrottle = 0;
			}
			#endregion

			#region Forward Thruster Control
			if (shipCurrentVel.z <= shipSettings.cruiseSpeed * shipSettings.forwardThrottle && shipSettings.forwardThrottle != 0)
			{
				shipRB.AddRelativeForce(0, 0, shipSettings.forwardForce * Time.deltaTime, ForceMode.Impulse);
			}
			else if (shipCurrentVel.z >= shipSettings.cruiseSpeed * shipSettings.forwardThrottle && shipCurrentVel.z > 0.5 && shipSettings.backThrottle == 0)
			{
				shipRB.AddRelativeForce(0, 0, -shipSettings.RCSForce * Time.deltaTime, ForceMode.Impulse);
			}
			#endregion

			#region Backward Thruster Control

			if (shipCurrentVel.z >= shipSettings.RCSSpeed * shipSettings.backThrottle && shipSettings.backThrottle != 0)
			{
				shipRB.AddRelativeForce(0, 0, -shipSettings.RCSForce * Time.deltaTime, ForceMode.Impulse);
			}
			else if(shipCurrentVel.z <= shipSettings.RCSSpeed * shipSettings.backThrottle && shipSettings.forwardThrottle == 0 && shipCurrentVel.z < -0.5f)
			{
				shipRB.AddRelativeForce(0, 0, shipSettings.RCSForce * Time.deltaTime, ForceMode.Impulse);
			}
			#endregion

			#region Up Thruster Control
			if (shipCurrentVel.y  <= shipSettings.RCSSpeed * shipSettings.upThrottle && shipSettings.upThrottle != 0)
			{
				shipRB.AddRelativeForce(0, shipSettings.RCSForce * Time.deltaTime, 0, ForceMode.Impulse);
			}
			else if (shipCurrentVel.y >= shipSettings.RCSSpeed * shipSettings.upThrottle && shipSettings.downThrottle == 0 && shipCurrentVel.y > 0.5f)
			{
				shipRB.AddRelativeForce(0, -shipSettings.RCSForce * Time.deltaTime, 0, ForceMode.Impulse);
			}
			#endregion

			#region Down Thruster Control
			if (shipCurrentVel.y >= shipSettings.RCSSpeed * shipSettings.downThrottle && shipSettings.downThrottle != 0)
			{
				shipRB.AddRelativeForce(0, -shipSettings.RCSForce * Time.deltaTime, 0, ForceMode.Impulse);
			}
			else if (shipCurrentVel.y <= shipSettings.RCSSpeed * shipSettings.downThrottle && shipSettings.upThrottle == 0 && shipCurrentVel.y < -0.5f)
			{
				shipRB.AddRelativeForce(0, shipSettings.RCSForce * Time.deltaTime, 0, ForceMode.Impulse);
			}
			#endregion

			#region Right Thruster Control
			if (shipCurrentVel.x <= shipSettings.RCSSpeed * shipSettings.starboardThrottle && shipSettings.starboardThrottle != 0)
			{
				shipRB.AddRelativeForce(shipSettings.RCSForce * Time.deltaTime, 0, 0, ForceMode.Impulse);
			}
			else if (shipCurrentVel.x >= shipSettings.RCSSpeed * shipSettings.starboardThrottle && shipSettings.portThrottle == 0 && shipCurrentVel.x > 0.5f)
			{
				shipRB.AddRelativeForce(-shipSettings.RCSForce * Time.deltaTime, 0, 0, ForceMode.Impulse);
			}
			#endregion

			#region Left Thruster Control
			if (shipCurrentVel.x >= shipSettings.RCSSpeed * shipSettings.portThrottle && shipSettings.portThrottle != 0)
			{
				shipRB.AddRelativeForce(-shipSettings.RCSForce * Time.deltaTime, 0, 0, ForceMode.Impulse);
			}
			else if (shipCurrentVel.x <= shipSettings.RCSSpeed * shipSettings.portThrottle && shipSettings.starboardThrottle == 0 && shipCurrentVel.x < -0.5f)
			{
				shipRB.AddRelativeForce(shipSettings.RCSForce * Time.deltaTime, 0, 0, ForceMode.Impulse);
			}
			#endregion

			#region Roll
			Vector3 currentAngularVel = shipRB.transform.InverseTransformDirection(shipRB.angularVelocity);

			Vector3 newTorque = new Vector3(0, 0, (Input.GetAxis("Roll") * shipSettings.RCSForce) * Time.deltaTime);

			shipRB.AddRelativeTorque(newTorque, ForceMode.Impulse);
			#endregion

			rb.velocity = Vector3.zero;
			transform.localPosition = fakePlayer.transform.localPosition;
			transform.localRotation = fakePlayer.transform.localRotation;
		}
		#endregion
	}


	#region Triggers
	void OnTriggerEnter(Collider col)
	{
		if (col.tag == "ShipGravity")
		{
			EnterShip(col);
		}
		else if (col.tag == "ControlPoint")
		{
			inControlPoint = true;

			ctrlSettings = col.GetComponent<ControlPointSettings>();
		}
	}

	void OnTriggerExit(Collider col)
	{
		if (col.tag == "ShipGravity")
		{
			ExitShip();
		}
		else if (col.tag == "ControlPoint")
		{
			inControlPoint = false;

			if (inShipControl)
			{
				LeaveControl(ctrlSettings);
			}

			ctrlSettings = null;
		}
	}
	#endregion

	#region Ship Enter/Exit
	private void ExitShip()
	{
		Log.Add("ExitShip");

		rb.isKinematic = false;

		onShip = false;
		DestroyObject(fakePlayer);
		transform.SetParent(null, true);

		fakeShip.SetActive(false);

		if (gravOnExit)
			rb.useGravity = true;

		GetComponent<CapsuleCollider>().isTrigger = false;
	}

	private void EnterShip(Collider col)
	{
		Log.Add("EnterShip");

		rb.isKinematic = true;

		onShip = true;
		transform.SetParent(col.transform.parent, true);

		fakeShip = transform.parent.GetComponent<ShipController>().fakeShip;

		fakePlayer = Instantiate(fakePlayerPrefab);
		fakePlayer.transform.parent = fakeShip.transform;

		fakePlayer.transform.localPosition = transform.localPosition;
		fakePlayer.transform.localRotation = transform.localRotation;

		fakeShip.SetActive(true);

		rb.useGravity = false;

		GetComponent<CapsuleCollider>().isTrigger = true;
	}
	#endregion

	#region Control Points
	private void TakeControl(ControlPointSettings settings)
	{
		if (!settings.occupied)
		{
			Log.Add("Taking Control");

			settings.occupied = true;

			shipToControl = settings.transform.parent.gameObject;

			shipSettings = settings.transform.parent.gameObject.GetComponent<ShipController>();

			shipRB = settings.transform.parent.gameObject.GetComponent<Rigidbody>();

			inShipControl = true;

			fakePlayer.GetComponent<FakePlayerController>().unlocked = false;
		}
		else
		{
			Log.AddError("Control Point Occupied");
		}
	}

	private void LeaveControl(ControlPointSettings settings)
	{
		if (inShipControl)
		{
			Log.Add("Leaving Control");

			settings.occupied = false;

			shipToControl = null;

			shipSettings = null;

			shipRB = null;

			inShipControl = false;

			fakePlayer.GetComponent<FakePlayerController>().unlocked = true;
		}
		else
		{
			Log.AddError("LeaveControl, not in control of ship");
		}
	}
	#endregion
}