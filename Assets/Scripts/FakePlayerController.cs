﻿using UnityEngine;
using System.Collections;

public class FakePlayerController : MonoBehaviour
{
	private Rigidbody rb;

	public bool _unlocked = true;

	public bool unlocked
	{
		set
		{
			_unlocked = value;

			rb.isKinematic = !value;
		}
		get
		{
			return _unlocked;
		}
	}

	void Start()
	{
		rb = GetComponent<Rigidbody>();
	}

	void Update()
	{
		if (unlocked)
		{
			Vector3 currenntVel = transform.InverseTransformDirection(rb.velocity);

			Vector3 newVelocity = new Vector3(Input.GetAxis("Horizontal"), currenntVel.y, Input.GetAxis("Vertical"));

			if (Input.GetKeyDown("space"))
			{
				newVelocity = new Vector3(currenntVel.x, 3, currenntVel.z);
			}

			rb.velocity = transform.TransformDirection(newVelocity); 
		}
	}
}
