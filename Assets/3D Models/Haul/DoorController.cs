﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
	public Animation DoorAnimation;


	public bool doorOpen;

	// Use this for initialization
	void Start()
	{
		if (DoorAnimation == null)
		{
			DoorAnimation = GetComponent<Animation>();
		}
	}

	// Update is called once per frame
	void Update()
	{
		if (!DoorAnimation.isPlaying)
		{
			if (Input.GetKeyDown(KeyCode.P) && !doorOpen)
			{
				doorOpen = true;
				DoorAnimation.Play("Ramp Close");
			}

			if (Input.GetKeyDown(KeyCode.P) && doorOpen)
			{
				doorOpen = false;
				DoorAnimation.Play("Ramp Open");
			} 
		}
	}
}
