﻿using UnityEngine;
using System.Collections;

public class ShipController : MonoBehaviour
{
	public Rigidbody rb;

	private void Start()
	{
		rb = GetComponent<Rigidbody>();

		rb.centerOfMass = Vector3.zero;
	}

	public int shipID;

	public string shipName;
	public GameObject fakeShip;

	public float cruiseSpeed;
	public float RCSSpeed;

	public float forwardForce;
	public float RCSForce;

	[Range(0, 1)]
	public float forwardThrottle = 0;
	[Range(0, -1)]
	public float backThrottle = 0;
	[Range(0, 1)]
	public float upThrottle = 0;
	[Range(0, -1)]
	public float downThrottle = 0;
	[Range(0, 1)]
	public float starboardThrottle = 0;
	[Range(0, -1)]
	public float portThrottle = 0;

	public float currentSpeed;

	private void Update()
	{
		currentSpeed = rb.velocity.magnitude;

		if (IsStationary())
		{
			rb.velocity = Vector3.zero;
		}
	}

	private bool IsStationary()
	{
		return rb.velocity.magnitude <= 0.5
			 && forwardThrottle == 0
			 && backThrottle == 0
			 && upThrottle == 0
			 && downThrottle == 0
			 && starboardThrottle == 0
			 && portThrottle == 0;
	}
}
