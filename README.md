# Space Game Prototype #

This project is a testing ground for mechanics and systems required to create a first person centric space sim.

### How to use ###
Simply clone the repo and open the project in unity. The only scene contains a prototype ship and player controller that can move from the platform to the ships gravity field with ease.